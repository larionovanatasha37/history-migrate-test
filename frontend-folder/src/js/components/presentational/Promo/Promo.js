import React from 'react';
import styles from './Promo.scss';

export class Promo extends React.Component{
  state = {
    top: 0
  };
  componentDidMount = () => {
    window.addEventListener('scroll', ()=>{
      this.setState({
        top: document.documentElement.scrollTop / 10
      });
    })
  };
  render = () => {
    return(
      <div className={styles.Promo}>
        <main style={{transform: `translateY(${this.state.top}px)`}}>totoro.js</main>
        <div className={styles.Arrow}/>
      </div>
    );
  }
}