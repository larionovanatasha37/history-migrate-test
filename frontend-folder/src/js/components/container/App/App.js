import React from 'react';
import styles from './App.scss';
import {Promo} from "../../presentational/Promo/Promo";

export class App extends React.Component {
  render = () => {
    return (
      <div className={styles.App}>
        <Promo/>
        <div className={styles.Content}>test</div>
      </div>
    );
  }
}