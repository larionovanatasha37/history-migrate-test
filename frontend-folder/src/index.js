import React from 'react';
import ReactDOM from 'react-dom';
import {App} from "./js/components/container/App/App";
import {Provider} from 'react-redux';
import {HashRouter} from 'react-router-dom';
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import managerReducer from './js/reducers/manager';
import './normalize.css';

const store = createStore(managerReducer, applyMiddleware(thunk));

ReactDOM.render(
<Provider store={store}>
  <HashRouter>
    <App />
  </HashRouter>
</Provider>, document.getElementById('root')
);